var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    "Well Name" : String,
    "Time" : Date,
    "Annulus pressure" : String,
    "Current" : String,
    "Discharge pressure" : String,
    "FTHP" : String,
    "FTHT" : String,
    "Flow rate" : String,
    "Gas flow rate" : String,
    "Frequency" : String,
    "Liquid flow rate" : String,
    "Motor temp" : String,
    "Status" : String,
    "Suction pressure" : String,
    "Suction temp" : String,
    "Vibration" : String,
    "Voltage" : String,
    "Well" : String,
    "CEIL_UP_PROD_DATE" : String,
    "PROD_DATE" : String,
    "Activity_Surface_Status" : String,
    "Activity_Potential_Code" : String,
    "Activity" : String,
    "Activity_Remarks" :String,
});

mongoose.model('sensor', SensorSchema);