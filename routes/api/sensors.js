var mongoose = require('mongoose');
var router = require('express').Router();
var moment = require("moment")
var auth = require('../auth');
var axios = require('axios');
var sensors = mongoose.model("sensor")
var EspSensorPredict = require("./EspSensorPredict.json")
const fs = require('fs')
var parse = require('csv-parse/lib/sync');

router.post("/EspSensor", function(req, res, next){
  var qry = {}
  if (typeof WellName !== undefined && typeof start !== undefined && typeof end !== undefined){
    req.body.start = req.body.start.split(" ")[0]
    req.body.end = req.body.end.split(" ")[0]
    qry["Well Name"] = req.body.WellName
    qry.Time = {}
    qry.Time['$gte'] = new Date(req.body.start + "T00:00:00.000+00:00")
    qry.Time['$lte'] = new Date(req.body.end + "T23:59:59.999+00:00")


    var request = {
      Wellname: req.body.WellName,
      start: new Date(req.body.start + "T00:00:00.000+00:00"),
      stop: new Date(req.body.end + "T23:59:59.999+00:00")
    }
    
    sensors.find(qry).then(function(result) {
    // get predict
      axios.post('http://192.168.2.189:8800/predict', {
        request: request
      })
      .then(data => {
        if (result.length > 0){
          data.data.Time = moment(result[result.length -1].Time).add(5, 'minutes');
          return res.json({
            data: result,
            length: result.length,
            predict: [data.data], 
            lengthPredict: [data.data].length
          })
        }else {
          return res.json({
            data: [],
            length: 0,
            predict: [], 
            lengthPredict: 0
          })
        }
      })
      .catch(error => res.json({
        error: error,
        status: false
      }))
      // return res.json({
      //   data: result,
      //   length: result.length,
      // })
    }).catch(function(e){
      return res.json(e);

    })
  }
})

router.post('/EspSensorPredict', function(req, res, next){
  return res.json(EspSensorPredict)
})

router.get('/EspSensorDevice', function (req, res, next){
  sensors.distinct("Well Name", function(err, result) {
    if (err){
      return res.json(err)
    }
    return res.json({
      device: result,
      length: result.length
    })
  })
})

router.get('/train', function(req, res, next){
  fs.readFile("./req_resample.csv", 'utf8' , function(err, data) {
    if (err) {
      console.error(err)
      return
    }
    const records = parse(data, {columns: true});
    var dataout = records.map((item ,index ) => {
      return item.Value
    })
    return res.json(dataout)
  })
})

module.exports = router;
